EESchema Schematic File Version 5
EELAYER 34 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "STM8 Breakout"
Date "2020-06-06"
Rev "20w23a"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 4250 3900
Connection ~ 6000 3750
Connection ~ 5300 3750
Connection ~ 7850 3100
Connection ~ 7850 3600
Connection ~ 4000 1300
Connection ~ 3850 1300
Connection ~ 7750 3000
Connection ~ 4850 1300
Connection ~ 4300 1950
NoConn ~ 6100 1850
NoConn ~ 5700 2050
Wire Wire Line
	2650 5500 2650 5850
Wire Wire Line
	2650 6150 2650 6300
Wire Wire Line
	2700 5400 2950 5400
Wire Wire Line
	2750 3300 2950 3300
Wire Wire Line
	2750 3400 2950 3400
Wire Wire Line
	2750 3500 2950 3500
Wire Wire Line
	2750 3700 2950 3700
Wire Wire Line
	2750 3800 2950 3800
Wire Wire Line
	2750 3900 2950 3900
Wire Wire Line
	2750 4000 2950 4000
Wire Wire Line
	2750 4100 2950 4100
Wire Wire Line
	2750 4200 2950 4200
Wire Wire Line
	2750 4300 2950 4300
Wire Wire Line
	2750 4400 2950 4400
Wire Wire Line
	2750 4600 2950 4600
Wire Wire Line
	2750 4700 2950 4700
Wire Wire Line
	2750 4800 2950 4800
Wire Wire Line
	2750 4900 2950 4900
Wire Wire Line
	2750 5000 2950 5000
Wire Wire Line
	2750 5100 2950 5100
Wire Wire Line
	2750 5200 2950 5200
Wire Wire Line
	2950 5500 2650 5500
Wire Wire Line
	3550 3000 3800 3000
Wire Wire Line
	3550 5800 3550 6000
Wire Wire Line
	3750 1300 3850 1300
Wire Wire Line
	3850 1300 3850 1500
Wire Wire Line
	3850 1300 4000 1300
Wire Wire Line
	3850 1800 3850 1950
Wire Wire Line
	3850 1950 4300 1950
Wire Wire Line
	4000 1300 4000 1400
Wire Wire Line
	4150 3700 4350 3700
Wire Wire Line
	4150 3800 4150 3700
Wire Wire Line
	4150 3900 4250 3900
Wire Wire Line
	4150 4000 4350 4000
Wire Wire Line
	4150 4100 4350 4100
Wire Wire Line
	4150 4200 4350 4200
Wire Wire Line
	4150 4300 4350 4300
Wire Wire Line
	4150 4400 4350 4400
Wire Wire Line
	4150 4500 4350 4500
Wire Wire Line
	4150 4700 4350 4700
Wire Wire Line
	4150 4900 4350 4900
Wire Wire Line
	4250 3800 4350 3800
Wire Wire Line
	4250 3900 4250 3800
Wire Wire Line
	4250 3900 4350 3900
Wire Wire Line
	4300 1700 4300 1950
Wire Wire Line
	4300 1950 4300 2100
Wire Wire Line
	4300 1950 4850 1950
Wire Wire Line
	4600 1300 4850 1300
Wire Wire Line
	4600 1500 4650 1500
Wire Wire Line
	4850 1300 4850 1500
Wire Wire Line
	4850 1300 5000 1300
Wire Wire Line
	4850 1800 4850 1950
Wire Wire Line
	5200 5450 5350 5450
Wire Wire Line
	5200 5800 5350 5800
Wire Wire Line
	5200 6150 5350 6150
Wire Wire Line
	5300 3000 5300 3250
Wire Wire Line
	5300 3550 5300 3750
Wire Wire Line
	5300 3750 5300 3950
Wire Wire Line
	5300 3750 5500 3750
Wire Wire Line
	5300 4250 5300 4500
Wire Wire Line
	5650 5800 5750 5800
Wire Wire Line
	5750 5450 5650 5450
Wire Wire Line
	5750 5600 5750 5450
Wire Wire Line
	5750 6000 5750 6150
Wire Wire Line
	5750 6150 5650 6150
Wire Wire Line
	5800 2050 6250 2050
Wire Wire Line
	5800 3750 6000 3750
Wire Wire Line
	6000 3000 6000 3250
Wire Wire Line
	6000 3550 6000 3750
Wire Wire Line
	6000 3750 6000 3950
Wire Wire Line
	6000 4250 6000 4500
Wire Wire Line
	6100 1450 6250 1450
Wire Wire Line
	6100 1650 6100 1750
Wire Wire Line
	6150 5800 6200 5800
Wire Wire Line
	6800 1150 6900 1150
Wire Wire Line
	6900 1150 6900 1250
Wire Wire Line
	6900 1550 6900 1700
Wire Wire Line
	6900 2000 6900 2100
Wire Wire Line
	7350 3000 7750 3000
Wire Wire Line
	7350 3100 7850 3100
Wire Wire Line
	7350 3200 7450 3200
Wire Wire Line
	7350 3300 7450 3300
Wire Wire Line
	7350 3600 7850 3600
Wire Wire Line
	7350 3700 7450 3700
Wire Wire Line
	7350 3800 7450 3800
Wire Wire Line
	7350 3900 7450 3900
Wire Wire Line
	7350 4000 7450 4000
Wire Wire Line
	7350 4100 7450 4100
Wire Wire Line
	7350 4200 7450 4200
Wire Wire Line
	7350 4300 7450 4300
Wire Wire Line
	7350 4400 7450 4400
Wire Wire Line
	7350 4500 7450 4500
Wire Wire Line
	7350 4600 7450 4600
Wire Wire Line
	7350 4700 7450 4700
Wire Wire Line
	7350 4800 7450 4800
Wire Wire Line
	7350 4900 7450 4900
Wire Wire Line
	7350 5000 7450 5000
Wire Wire Line
	7650 3400 7650 3500
Wire Wire Line
	7650 3500 7350 3500
Wire Wire Line
	7750 3000 7750 3400
Wire Wire Line
	7750 3000 8200 3000
Wire Wire Line
	7750 3400 7650 3400
Wire Wire Line
	7850 3100 7850 3600
Wire Wire Line
	7850 3100 8200 3100
Wire Wire Line
	7850 3600 8050 3600
Wire Wire Line
	7950 3500 8050 3500
Wire Wire Line
	7950 3700 8050 3700
Wire Wire Line
	7950 3800 8050 3800
Wire Wire Line
	7950 3900 8050 3900
Wire Wire Line
	7950 4000 8050 4000
Wire Wire Line
	7950 4100 8050 4100
Wire Wire Line
	7950 4200 8050 4200
Wire Wire Line
	7950 4300 8050 4300
Wire Wire Line
	7950 4400 8050 4400
Wire Wire Line
	7950 4500 8050 4500
Wire Wire Line
	7950 4600 8050 4600
Wire Wire Line
	7950 4700 8050 4700
Wire Wire Line
	7950 4800 8050 4800
Wire Wire Line
	7950 4900 8050 4900
Wire Wire Line
	7950 5000 8050 5000
Wire Wire Line
	8200 3100 8200 3200
Wire Notes Line
	2250 2650 3550 2650
Wire Notes Line
	2250 6650 2250 2650
Wire Notes Line
	3550 2650 4750 2650
Wire Notes Line
	3600 900  3600 2450
Wire Notes Line
	3600 900  5350 900 
Wire Notes Line
	4750 2650 4750 6650
Wire Notes Line
	4750 6650 2250 6650
Wire Notes Line
	4850 5100 5500 5100
Wire Notes Line
	4850 6350 4850 5100
Wire Notes Line
	5050 2650 5300 2650
Wire Notes Line
	5050 4850 5050 2650
Wire Notes Line
	5300 2650 6400 2650
Wire Notes Line
	5350 900  5350 2450
Wire Notes Line
	5350 900  6400 900 
Wire Notes Line
	5350 2450 3600 2450
Wire Notes Line
	5500 5100 6550 5100
Wire Notes Line
	6400 900  6400 2450
Wire Notes Line
	6400 900  7300 900 
Wire Notes Line
	6400 2450 5350 2450
Wire Notes Line
	6400 2650 6400 4850
Wire Notes Line
	6400 4850 5050 4850
Wire Notes Line
	6550 5100 6550 6350
Wire Notes Line
	6550 6350 4850 6350
Wire Notes Line
	6650 2650 8700 2650
Wire Notes Line
	6650 3150 6650 2650
Wire Notes Line
	6650 3150 6650 5200
Wire Notes Line
	7300 900  7300 2450
Wire Notes Line
	7300 2450 6400 2450
Wire Notes Line
	8700 2650 8700 5200
Wire Notes Line
	8700 5200 6650 5200
Text Notes 2300 2750 0    50   ~ 0
Microprocessor
Text Notes 3650 1000 0    50   ~ 0
Voltage Regulator
Text Notes 4900 5200 0    50   ~ 0
Output LED
Text Notes 5100 2750 0    50   ~ 0
External Timer
Text Notes 5400 1000 0    50   ~ 0
USB Power In
Text Notes 6450 1000 0    50   ~ 0
Power ON LED
Text Notes 6700 2750 0    50   ~ 0
Output Pins
Text Notes 7000 7100 0    50   ~ 0
Copyright Rob J Heywood 2020.\nThis source describes Open Hardware and is licensed under the CERN-OHL-Pv2. You may redistribute and\nmodify this documentation and make products using it under the terms of the CERN-OHL-P v2\n(https:/cern.ch/cern-ohl). This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,\nINCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE.\nPlease see the CERN-OHL-P v2 for applicable conditions.\nhttps://www.robjh.com/hw/stm8
Text GLabel 2700 5400 0    50   Input ~ 0
RESET
Text GLabel 2750 3300 0    50   Input ~ 0
PA1
Text GLabel 2750 3400 0    50   Input ~ 0
PA2
Text GLabel 2750 3500 0    50   Input ~ 0
PA3
Text GLabel 2750 3700 0    50   Input ~ 0
PB0
Text GLabel 2750 3800 0    50   Input ~ 0
PB1
Text GLabel 2750 3900 0    50   Input ~ 0
PB2
Text GLabel 2750 4000 0    50   Input ~ 0
PB3
Text GLabel 2750 4100 0    50   Input ~ 0
PB4
Text GLabel 2750 4200 0    50   Input ~ 0
PB5
Text GLabel 2750 4300 0    50   Input ~ 0
PB6
Text GLabel 2750 4400 0    50   Input ~ 0
PB7
Text GLabel 2750 4600 0    50   Input ~ 0
PC1
Text GLabel 2750 4700 0    50   Input ~ 0
PC2
Text GLabel 2750 4800 0    50   Input ~ 0
PC3
Text GLabel 2750 4900 0    50   Input ~ 0
PC4
Text GLabel 2750 5000 0    50   Input ~ 0
PC5
Text GLabel 2750 5100 0    50   Input ~ 0
PC6
Text GLabel 2750 5200 0    50   Input ~ 0
PC7
Text GLabel 3800 3000 2    50   Input ~ 0
VDD
Text GLabel 4350 3700 2    50   Input ~ 0
PD0
Text GLabel 4350 3800 2    50   Input ~ 0
PD1
Text GLabel 4350 3900 2    50   Input ~ 0
SWIM
Text GLabel 4350 4000 2    50   Input ~ 0
PD2
Text GLabel 4350 4100 2    50   Input ~ 0
PD3
Text GLabel 4350 4200 2    50   Input ~ 0
PD4
Text GLabel 4350 4300 2    50   Input ~ 0
PD5
Text GLabel 4350 4400 2    50   Input ~ 0
PD6
Text GLabel 4350 4500 2    50   Input ~ 0
PD7
Text GLabel 4350 4700 2    50   Input ~ 0
PE5
Text GLabel 4350 4900 2    50   Input ~ 0
PF4
Text GLabel 5000 1300 2    50   Input ~ 0
VDD
Text GLabel 5200 5450 0    50   Input ~ 0
PB6
Text GLabel 5200 5800 0    50   Input ~ 0
PB7
Text GLabel 5200 6150 0    50   Input ~ 0
PF4
Text GLabel 5300 3000 1    50   Input ~ 0
PA1
Text GLabel 6000 3000 1    50   Input ~ 0
PA2
Text GLabel 6200 5800 2    50   Input ~ 0
VDD
Text GLabel 6800 1150 0    50   Input ~ 0
VDD
Text GLabel 7450 3200 2    50   Input ~ 0
SWIM
Text GLabel 7450 3300 2    50   Input ~ 0
RESET
Text GLabel 7450 3700 2    50   Input ~ 0
PD0
Text GLabel 7450 3800 2    50   Input ~ 0
PD1
Text GLabel 7450 3900 2    50   Input ~ 0
PD2
Text GLabel 7450 4000 2    50   Input ~ 0
PD3
Text GLabel 7450 4100 2    50   Input ~ 0
PD4
Text GLabel 7450 4200 2    50   Input ~ 0
PD5
Text GLabel 7450 4300 2    50   Input ~ 0
PD6
Text GLabel 7450 4400 2    50   Input ~ 0
PD7
Text GLabel 7450 4500 2    50   Input ~ 0
PA1
Text GLabel 7450 4600 2    50   Input ~ 0
PA2
Text GLabel 7450 4700 2    50   Input ~ 0
PA3
Text GLabel 7450 4800 2    50   Input ~ 0
PF4
Text GLabel 7450 4900 2    50   Input ~ 0
PB7
Text GLabel 7450 5000 2    50   Input ~ 0
PB6
Text GLabel 7950 3500 0    50   Input ~ 0
VDD
Text GLabel 7950 3700 0    50   Input ~ 0
PC7
Text GLabel 7950 3800 0    50   Input ~ 0
PC6
Text GLabel 7950 3900 0    50   Input ~ 0
PC5
Text GLabel 7950 4000 0    50   Input ~ 0
PC4
Text GLabel 7950 4100 0    50   Input ~ 0
PC3
Text GLabel 7950 4200 0    50   Input ~ 0
PC2
Text GLabel 7950 4300 0    50   Input ~ 0
PC1
Text GLabel 7950 4400 0    50   Input ~ 0
PE5
Text GLabel 7950 4500 0    50   Input ~ 0
PB0
Text GLabel 7950 4600 0    50   Input ~ 0
PB1
Text GLabel 7950 4700 0    50   Input ~ 0
PB2
Text GLabel 7950 4800 0    50   Input ~ 0
PB3
Text GLabel 7950 4900 0    50   Input ~ 0
PB4
Text GLabel 7950 5000 0    50   Input ~ 0
PB5
$Comp
L power:+5V #PWR0109
U 1 1 5EBB6607
P 3750 1300
F 0 "#PWR0109" H 3750 1150 50  0001 C CNN
F 1 "+5V" H 3765 1470 50  0000 C CNN
F 2 "" H 3750 1300 50  0001 C CNN
F 3 "" H 3750 1300 50  0001 C CNN
	1    3750 1300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0111
U 1 1 5EBB457E
P 6250 1450
F 0 "#PWR0111" H 6250 1300 50  0001 C CNN
F 1 "+5V" H 6265 1620 50  0000 C CNN
F 2 "" H 6250 1450 50  0001 C CNN
F 3 "" H 6250 1450 50  0001 C CNN
	1    6250 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0107
U 1 1 5EBB28E6
P 8200 3000
F 0 "#PWR0107" H 8200 2850 50  0001 C CNN
F 1 "+5V" H 8215 3170 50  0000 C CNN
F 2 "" H 8200 3000 50  0001 C CNN
F 3 "" H 8200 3000 50  0001 C CNN
	1    8200 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5EB57603
P 2650 6300
F 0 "#PWR0101" H 2650 6050 50  0001 C CNN
F 1 "GND" H 2655 6130 50  0000 C CNN
F 2 "" H 2650 6300 50  0001 C CNN
F 3 "" H 2650 6300 50  0001 C CNN
	1    2650 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5EB45E77
P 3550 6000
F 0 "#PWR0102" H 3550 5750 50  0001 C CNN
F 1 "GND" H 3555 5830 50  0000 C CNN
F 2 "" H 3550 6000 50  0001 C CNN
F 3 "" H 3550 6000 50  0001 C CNN
	1    3550 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5EBB2152
P 4300 2100
F 0 "#PWR0108" H 4300 1850 50  0001 C CNN
F 1 "GND" H 4305 1930 50  0000 C CNN
F 2 "" H 4300 2100 50  0001 C CNN
F 3 "" H 4300 2100 50  0001 C CNN
	1    4300 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5EB6BA1E
P 5300 4500
F 0 "#PWR0105" H 5300 4250 50  0001 C CNN
F 1 "GND" H 5305 4330 50  0000 C CNN
F 2 "" H 5300 4500 50  0001 C CNN
F 3 "" H 5300 4500 50  0001 C CNN
	1    5300 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5EB6C190
P 6000 4500
F 0 "#PWR0104" H 6000 4250 50  0001 C CNN
F 1 "GND" H 6005 4330 50  0000 C CNN
F 2 "" H 6000 4500 50  0001 C CNN
F 3 "" H 6000 4500 50  0001 C CNN
	1    6000 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5EBB4D15
P 6250 2050
F 0 "#PWR0110" H 6250 1800 50  0001 C CNN
F 1 "GND" H 6255 1880 50  0000 C CNN
F 2 "" H 6250 2050 50  0001 C CNN
F 3 "" H 6250 2050 50  0001 C CNN
	1    6250 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5EB7163E
P 6900 2100
F 0 "#PWR0106" H 6900 1850 50  0001 C CNN
F 1 "GND" H 6905 1930 50  0000 C CNN
F 2 "" H 6900 2100 50  0001 C CNN
F 3 "" H 6900 2100 50  0001 C CNN
	1    6900 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5EB548AE
P 8200 3200
F 0 "#PWR0103" H 8200 2950 50  0001 C CNN
F 1 "GND" H 8205 3027 50  0000 C CNN
F 2 "" H 8200 3200 50  0001 C CNN
F 3 "" H 8200 3200 50  0001 C CNN
	1    8200 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R TR1
U 1 1 5EB70FBE
P 5300 3400
F 0 "TR1" H 5370 3445 50  0000 L CNN
F 1 "0" H 5370 3355 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5230 3400 50  0001 C CNN
F 3 "~" H 5300 3400 50  0001 C CNN
F 4 "-" H 5300 3400 50  0001 C CNN "Farnell"
	1    5300 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R RgbR1
U 1 1 5EB3E13A
P 5500 5450
F 0 "RgbR1" V 5700 5450 50  0000 C CNN
F 1 "24" V 5615 5450 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5430 5450 50  0001 C CNN
F 3 "~" H 5500 5450 50  0001 C CNN
F 4 "2072833" V 5500 5450 50  0001 C CNN "Farnell"
	1    5500 5450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R rGbR1
U 1 1 5EB3E719
P 5500 5800
F 0 "rGbR1" V 5706 5800 50  0000 C CNN
F 1 "3.3" V 5615 5800 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5430 5800 50  0001 C CNN
F 3 "~" H 5500 5800 50  0001 C CNN
F 4 "2072501" V 5500 5800 50  0001 C CNN "Farnell"
	1    5500 5800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R rgBR1
U 1 1 5EB4050E
P 5500 6150
F 0 "rgBR1" V 5706 6150 50  0000 C CNN
F 1 "3.3" V 5615 6150 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5430 6150 50  0001 C CNN
F 3 "~" H 5500 6150 50  0001 C CNN
F 4 "2072501" V 5500 6150 50  0001 C CNN "Farnell"
	1    5500 6150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R TR2
U 1 1 5EB64C48
P 6000 3400
F 0 "TR2" H 6070 3445 50  0000 L CNN
F 1 "60" H 6070 3355 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5930 3400 50  0001 C CNN
F 3 "~" H 6000 3400 50  0001 C CNN
F 4 "2694642" H 6000 3400 50  0001 C CNN "Farnell"
	1    6000 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R PwrR1
U 1 1 5EB713BC
P 6900 1850
F 0 "PwrR1" H 6970 1895 50  0000 L CNN
F 1 "24" H 6970 1805 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6830 1850 50  0001 C CNN
F 3 "~" H 6900 1850 50  0001 C CNN
F 4 "2072833" H 6900 1850 50  0001 C CNN "Farnell"
	1    6900 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5EB7105A
P 6900 1400
F 0 "D1" H 6893 1616 50  0000 C CNN
F 1 "PwrLED" H 6892 1525 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 6900 1400 50  0001 C CNN
F 3 "~" H 6900 1400 50  0001 C CNN
F 4 "-" H 6900 1400 50  0001 C CNN "Farnell"
	1    6900 1400
	0    -1   -1   0   
$EndComp
$Comp
L Device:C VCAP1
U 1 1 5EB56CE1
P 2650 6000
F 0 "VCAP1" H 2765 6045 50  0000 L CNN
F 1 "680nF" H 2765 5955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2688 5850 50  0001 C CNN
F 3 "~" H 2650 6000 50  0001 C CNN
F 4 "2812262" H 2650 6000 50  0001 C CNN "Farnell"
	1    2650 6000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5EBB44F8
P 3850 1650
F 0 "C1" H 3965 1695 50  0000 L CNN
F 1 "1uF" H 3965 1605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3888 1500 50  0001 C CNN
F 3 "~" H 3850 1650 50  0001 C CNN
F 4 "2525034" H 3850 1650 50  0001 C CNN "Farnell"
	1    3850 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5EBB439F
P 4850 1650
F 0 "C2" H 4965 1695 50  0000 L CNN
F 1 "1uF" H 4965 1605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4888 1500 50  0001 C CNN
F 3 "~" H 4850 1650 50  0001 C CNN
F 4 "2525034" H 4850 1650 50  0001 C CNN "Farnell"
	1    4850 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:C TC1
U 1 1 5EB64426
P 5300 4100
F 0 "TC1" H 5415 4145 50  0000 L CNN
F 1 "20pF" H 5415 4055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5338 3950 50  0001 C CNN
F 3 "~" H 5300 4100 50  0001 C CNN
F 4 "1758948" H 5300 4100 50  0001 C CNN "Farnell"
	1    5300 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:C TC2
U 1 1 5EB63DFF
P 6000 4100
F 0 "TC2" H 6115 4145 50  0000 L CNN
F 1 "20pF" H 6115 4055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6038 3950 50  0001 C CNN
F 3 "~" H 6000 4100 50  0001 C CNN
F 4 "1758948" H 6000 4100 50  0001 C CNN "Farnell"
	1    6000 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal TY1
U 1 1 5EB64F89
P 5650 3750
F 0 "TY1" H 5650 3483 50  0000 C CNN
F 1 "16MHz" H 5650 3574 50  0000 C CNN
F 2 "Crystal:Crystal_HC49-4H_Vertical" H 5650 3750 50  0001 C CNN
F 3 "~" H 5650 3750 50  0001 C CNN
F 4 "1611761" H 5650 3750 50  0001 C CNN "Farnell"
	1    5650 3750
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 ST-Link1
U 1 1 5EAAF1BC
P 7150 3100
F 0 "ST-Link1" H 7550 3050 50  0000 R CNN
F 1 "Conn_01x04" H 7068 3326 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Horizontal" H 7150 3100 50  0001 C CNN
F 3 "~" H 7150 3100 50  0001 C CNN
F 4 "-" H 7150 3100 50  0001 C CNN "Farnell"
	1    7150 3100
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED_ABRG status1
U 1 1 5EB342C7
P 5950 5800
F 0 "status1" H 5950 6296 50  0000 C CNN
F 1 "RGB" H 5950 6205 50  0000 C CNN
F 2 "custom-components:LED_RGB_MULTICOMP_MP001253.5x2.8mm" H 5950 5800 50  0001 C CNN
F 3 "~" H 5950 5800 50  0001 C CNN
F 4 "3265382" H 5950 5800 50  0001 C CNN "Farnell"
	1    5950 5800
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:MIC5205YM5 U2
U 1 1 5EBAF18A
P 4300 1400
F 0 "U2" H 4300 1741 50  0000 C CNN
F 1 "MIC5205YM5" H 4300 1650 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4300 1725 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20005785A.pdf" H 4300 1400 50  0001 C CNN
F 4 "2510377RL" H 4300 1400 50  0001 C CNN "Farnell"
	1    4300 1400
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J1
U 1 1 5EBB3C83
P 5800 1650
F 0 "J1" H 5855 2116 50  0000 C CNN
F 1 "USB_B_Micro" H 5855 2025 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-AB_Molex_47590-0001" H 5950 1600 50  0001 C CNN
F 3 "~" H 5950 1600 50  0001 C CNN
F 4 "1568022" H 5800 1650 50  0001 C CNN "Farnell"
	1    5800 1650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x16 Bank2
U 1 1 5EAAD671
P 7150 4200
F 0 "Bank2" H 7450 4200 50  0000 R CNN
F 1 "Conn_01x16" H 7700 4100 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 7150 4200 50  0001 C CNN
F 3 "~" H 7150 4200 50  0001 C CNN
F 4 "-" H 7150 4200 50  0001 C CNN "Farnell"
	1    7150 4200
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x16 Bank1
U 1 1 5EAAE758
P 8250 4200
F 0 "Bank1" H 8350 4200 50  0000 L CNN
F 1 "Conn_01x16" H 8330 4101 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 8250 4200 50  0001 C CNN
F 3 "~" H 8250 4200 50  0001 C CNN
F 4 "-" H 8250 4200 50  0001 C CNN "Farnell"
	1    8250 4200
	1    0    0    -1  
$EndComp
$Comp
L MCU_ST_STM8:STM8S003K3T U1
U 1 1 5EAAC663
P 3550 4400
F 0 "U1" H 3550 5981 50  0000 C CNN
F 1 "STM8S003K3T" H 3550 5890 50  0000 C CNN
F 2 "Package_QFP:LQFP-32_7x7mm_P0.8mm" H 3600 5900 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00024550.pdf" H 3550 4400 50  0001 C CNN
F 4 "2770278" H 3550 4400 50  0001 C CNN "Farnell"
	1    3550 4400
	1    0    0    -1  
$EndComp
$EndSCHEMATC
